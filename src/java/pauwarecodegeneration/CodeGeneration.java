package pauwarecodegeneration;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLMapImpl;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.FinalState;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.Vertex;

/**
 * This class is responsible for the generation of the PauWare (http://pauware.univ-pau.fr) code by parsing an xmi file.
 * 
 * @author Lea Brunschwig
 * @version 1.0
 */
public class CodeGeneration {
    
    /*
     * ATTRIBUTS
     */
    private String SM_class; // name of the generated state machine class
    private String SM_name; // name of the state machine
    private final List<State> state_list; // list of the found states
    private final List<Transition> trans_list; // list of the found transitions
    private final List<Operation> op_list; // list of the found operations
    private final List<Constraint> inv_list; // list of the found invariants
    private final List<Pseudostate> pseudostate_list; // list of the found pseudostates
    private Boolean businessObject; // this boolean allows to add a business object to the generated code if needed
    private Boolean businessGuard; // this boolean allows to add the business guard notion to the generated code if needed
    private Boolean atLeastOneEvent; // this boolean says if there is at least one event
    private Integer counterOfCompositeState; // used for counting the composite states
    private List<String> listOfVerticesInsideSM; // list of the states and pseudostates that are inside the state machine
    private final Boolean showOnSystemOut; // boolean that specifies if the PauWare trace will be printed in the console or not
    private final Boolean viewer; // boolean that specifies if the PauWare viewer will be added or not
    private final Boolean EEorME; // boolean that specifies if the Java version will be EE or ME
    private final String xmiName; // the name of the xmi that is going to be parsed
    private final Boolean timer; // boolean that specifies if the Timer interface needs to be added or not
    private Boolean isNameComposite; // boolean that specifies if the name of the states has to be named after their composite or not
    private Boolean isManageableNeeded; // boolean that specifies if the Manageable interface needs to be added or not
    private StateMachine sm = null; // singleton
    private String businessMethods; // string that contains all the business methods for the business object class
    private final String path; // path where the files are stocked
    private final HashMap<String, String> stringmap; // directory of all different files that are generated stringmap<nameOfFile, content>
    private final HashMap<Operation, Boolean> hm_operations; // list of all operations, true means the operation has been used somewhere is the state machine and has to be printed
    
    /**
     * CONSTRUCTOR
     * @param xmiName the name of the xmi that is going to be parsed
     * @param EEorME boolean that specifies if the Java version will be EE or ME
     * @param viewer boolean that specifies if the PauWare viewer will be added or not
     * @param trace boolean that specifies if the PauWare trace will be printed in the console or not
     * @param timer boolean that specifies if the Timer interface needs to be added or not
     * @param path path where the files are stocked
     */
    public CodeGeneration(String xmiName, Boolean EEorME, Boolean viewer, Boolean trace, Boolean timer, String path) {
        state_list = new ArrayList();
        trans_list = new ArrayList();
        op_list = new ArrayList();
        inv_list = new ArrayList();
        pseudostate_list = new ArrayList();
        SM_class = "";
        SM_name = "";
        businessObject = false;
        businessGuard = false;
        atLeastOneEvent = false;
        counterOfCompositeState = 0;
        listOfVerticesInsideSM = new ArrayList();
        this.xmiName = xmiName;
        this.EEorME = EEorME;
        this.viewer = viewer;
        showOnSystemOut = trace;
        this.timer = timer;
        this.path = path;
        stringmap = new HashMap<>();
        hm_operations = new HashMap<>();
    }
    
    
    /**
     * Allow to load the model that is contained by an .xmi file.
     * @param uri xmi path
     * @param pack instance of the package of the root element
     * @return 
     */
    private static Resource chargerModele(String uri, EPackage pack) {
        
	Resource resource = null;
	try {
	    URI uriUri = URI.createURI(uri);
	    Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
	    resource = (new ResourceSetImpl()).createResource(uriUri);
	    XMLResource.XMLMap xmlMap = new XMLMapImpl();
	    xmlMap.setNoNamespacePackage(pack);
            java.util.Map<String, XMLResource.XMLMap> options = new java.util.HashMap<>();
	    options.put(XMLResource.OPTION_XML_MAP, xmlMap);
	    resource.load(options);
        }
	catch(IOException e) {
	    System.err.println("ERREUR chargement du modèle : "+e);
	}
	return resource;
    }
    
    /**
     * Every generated file is saved in a map, in order to update them and access them we use write
     * @param key name of the file
     * @param text content
     * @param bool replace (false) or append (true)
     */
    private void write(String key, String text, Boolean bool) {
        StringBuilder sb = new StringBuilder();
        
        if(stringmap.get(key) == null) {
            stringmap.put(key, text);
        } else {
            if(bool) {
                stringmap.put(key, sb.append(stringmap.get(key)).append(text).toString());
            } else {
                stringmap.put(key, text);
            }
        }
    }
    
    /**
     * Extracts the element owned by a specific region and sorts them in different lists (state list, transition list, pseudostate list)
     * @param r region from which elements are extracted.
     */
    private void region(Region r) {
        
        /* Init */
        State s;
        Transition t;
        Pseudostate ps;
        
        /* Body */
        for(Element e : r.getOwnedElements()) { // we deal with all the element of the region
            // We deal with FinalState another way
            if (e instanceof State && !(e instanceof FinalState)) {
                s = (State)e;
                state_list.add(s);
                for(Element elt : s.getOwnedElements()) { // we recursively parse all the element of the found state
                    if(elt instanceof Region) {
                        r = (Region)elt;
                        region(r);
                    }
                }
            } else if (e instanceof Transition) {
                t = (Transition)e;
                trans_list.add(t);
            } else if (e instanceof Pseudostate) {
                ps = (Pseudostate)e;
                // since initial, final, deepHistory and shallowHistory are existing in the PauWare API, we won't deal with them the same way as the other pseudostates
                if(!ps.getKind().toString().equals("initial") && !ps.getKind().toString().equals("final") && !ps.getKind().toString().equals("deepHistory") && !ps.getKind().toString().equals("shallowHistory"))
                    pseudostate_list.add(ps);
            }
        }
    }
    
    /**
     * A given word got its first letter transform on UPPER or LOWER case.
     * @param word string to transform
     * @param bool boolean that specify if we want lower (true) or upper (false) case.
     * @return a string of the transformed word
     */
    private String firstLetterLowerOrUpperCase(String word, Boolean bool) {
        
        /* Init */
        StringBuilder sb = new StringBuilder();
        
        /* Body */
        if(bool) {
            sb.append(word.substring(0,1).toLowerCase());
        } else {
            sb.append(word.substring(0,1).toUpperCase());
        }
        sb.append(word.substring(1, word.length()));
        return sb.toString();
    }
    
    /**
     * Removes all spaces from a given string.
     * @param string 
     * @return string without space (" " -> "_")
     */
    private String removeSpace(String string) {
        if(!string.contains(" ")){
            return string;
        } else {
            return string.replace(" ", "_");
        }
    }
    
    /**
     * Replaces all the punctuation characters.
     * @param string with punctuation characters
     * @return string without punctuation characters
     */
    private String removePunctuation(String string) {
        if(string.contains(","))
            string = string.replace(",", "_comma_");
        if(string.contains("."))
            string = string.replace(".", "_dot_");
        if(string.contains(":"))
            string = string.replace(":", "_colon_");
        if(string.contains(";"))
            string = string.replace(";", "_semicolon_");
        if(string.contains("/"))
            string = string.replace("/", "_slash_");
        if(string.contains("\\"))
            string = string.replace("\\", "_antislash_");
        if(string.contains("-"))
            string = string.replace("-", "_dash_");
        if(string.contains("?"))
            string = string.replace("?", "_questionmark_");
        if(string.contains("!"))
            string = string.replace("!", "_exclamationmark");
        if(string.contains("\'"))
            string = string.replace("\'", "_apostrophe_");
        if(string.contains("\""))
            string = string.replace("\"", "_doublequotationmark_");
        if(string.contains("(") || string.contains(")") || string.contains("[") || string.contains("]") || string.contains("{") || string.contains("}")) {
            string = string.replace("(", "_bracket_");
            string = string.replace(")", "_bracket_");
            string = string.replace("[", "_bracket_");
            string = string.replace("]", "_bracket_");
            string = string.replace("{", "_bracket_");
            string = string.replace("}", "_bracket_");
        }
        return string;
    }
    
    /**
     * Transform a boolean from a guard or invariant in a string
     * @param string guard or invariant to transform
     * @return the string after transformation
     */
    private String translateBoolean(String string) {
        string = removeSpace(string);
        if(string.contains("=="))
            string = string.replace("==", "_is_equal_to_");
        if(string.contains("<="))
            string = string.replace("<=", "_is_less_than_or_equal_to_");
        if(string.contains(">="))
            string = string.replace(">=", "_is_greater_than_or_equal_to_");
        if(string.contains("="))
            string = string.replace("=", "_is_equal_to_");
        if(string.contains(">"))
            string = string.replace(">", "_is_greater_than_");
        if(string.contains("<"))
            string = string.replace("<", "_is_less_than_");
        if(string.contains("!"))
            string = string.replace("!", "NOT_");
        if(string.contains("("))
            string = string.replace("(", "opbr_");
        if(string.contains(")"))
            string = string.replace(")", "_clbr");
        if(string.contains("-"))
            string = string.replace("-", "minus");
        if(string.contains("+"))
            string = string.replace("+", "plus");
        if(string.contains("/"))
            string = string.replace("/", "_division_");
        if(string.contains("*"))
            string = string.replace("*", "_multiplication_");
        if(string.contains("%"))
            string = string.replace("%", "_modulo_");
        if(string.contains("."))
            string = string.replace(".", "_dot_");
        if(string.contains(","))
            string = string.replace(",", "_coma_");
        if(string.contains("&&"))
            string = string.replace("&&", "_AND_");
        if(string.contains("||"))
            string = string.replace("||", "_OR_");
        if(string.startsWith("0"))
            string = string.replace("0", "_0");
        if(string.startsWith("1"))
            string = string.replace("1", "_1");
        if(string.startsWith("2"))
            string = string.replace("2", "_2");
        if(string.startsWith("3"))
            string = string.replace("3", "_3");
        if(string.startsWith("4"))
            string = string.replace("4", "_4");
        if(string.startsWith("5"))
            string = string.replace("5", "_5");
        if(string.startsWith("6"))
            string = string.replace("6", "_6");
        if(string.startsWith("7"))
            string = string.replace("7", "_7");
        if(string.startsWith("8"))
            string = string.replace("8", "_8");
        if(string.startsWith("9"))
            string = string.replace("9", "_9");
        return string;
    }
    
    /**
     * Extract the type specified in the string.
     * @param string
     * @return the type
     */
    private String pickType(String string) {
        
        /* Init */
        String[] parts;
        
        /* Body */
        // in the xmi file it looks like : "<ownedParameter ...> <type xmi:type="..." href="pathmap://...#String"/>"
        if(string.contains("#")) {
            parts = string.split("#");
            string = parts[parts.length-1];
        } else if(string.contains(" ")) {
            parts = string.split(" ");
            string = parts[parts.length-1];
        }
        
        return string.substring(0, string.length()-1);
        
    }
    
    /**
     * Transform the name of a state in a root name, used to avoid states having the same name.
     * @param state the state we want to rename
     * @param compositeNameActive the transformation is active or not
     * @return the transformed state name
     */
    private String compositeName(State state, Boolean compositeNameActive) {
        
        /* Init */
        StringBuilder sb = new StringBuilder();
        Region regionOwningState;
        State stateOwningRegion;
        String name;
        
        /* Body */
        name = removePunctuation(removeSpace(state.getName()));
        if(name.equals("Composite") || name.equals("Pseudo-state") || name.equals("Pseudo-event")) {
            name = sb.append(name).toString();
            sb.delete(0, sb.length());
        }
        if(state.getOwner() != null && compositeNameActive) { // if state has an owner
            if(state.getOwner() instanceof Region) { // if state is owned by a region
                regionOwningState = (Region) state.getOwner();
                if(state.getOwner().getOwner() != null) { // if this region has an owner
                    if(state.getOwner().getOwner() instanceof State) { // and this region is owned by another state
                        stateOwningRegion = (State) state.getOwner().getOwner();
                        if(stateOwningRegion.getOwner() != null) {  // if this composite state has also an owner
                            if(stateOwningRegion.getOwner().getOwner() != null) { // and another owner above
                                if(stateOwningRegion.getOwner().getOwner() instanceof State) { // if it happens that this owner is a state
                                    // ["state"_in_"regionOwningState"_in_"stateOwningRegion"_in_...]
                                    sb.append(name).append("_in_").append(regionOwningState.getName()).append("_in_").append(stateOwningRegion.getName()).append("_in_").append(compositeName((State)stateOwningRegion.getOwner().getOwner(), compositeNameActive));
                                    name = sb.toString();
                                } else { // the owner has an owner but the another owner above is not a state
                                    // ["state"_in_"regionOwningState"_in_"stateOwningRegion"]
                                    sb.append(name).append("_in_").append(regionOwningState.getName()).append("_in_").append(stateOwningRegion.getName());
                                    name = sb.toString();
                                }
                            } else { // the composite state has an owner but not another above
                                // ["state"_in_"regionOwningState"_in_"stateOwningRegion"]
                                sb.append(name).append("_in_").append(regionOwningState.getName()).append("_in_").append(stateOwningRegion.getName());
                                name = sb.toString();
                            }
                        } else { // the composite state doesn't have an owner 
                            // ["state"_in_"regionOwningState"_in_"stateOwningRegion"]
                            sb.append(name).append("_in_").append(regionOwningState.getName()).append("_in_").append(stateOwningRegion.getName());
                            name = sb.toString();
                        }
                    }
                }
            }
        }
        return name;
        
    }
    
    /**
     * Creates a name for the state reprensantation of a pseudostate in PauWare
     * @param pseudostate 
     * @return the name for the state corresponding to the pseudostate
     */
    private String pseudoStateNameGenerator(Pseudostate pseudostate) {
        
        /* Init */
        StringBuilder sb = new StringBuilder();
        State stateOwningPseudostate, state;
        String name;
        Pseudostate otherPseudostate;
        
        /* Body */
        //name = pseudostate.getKind().toString();
        sb.append(pseudostate.getKind().toString());
        if(pseudostate.getOwner() != null) {
            if(pseudostate.getOwner().getOwner() != null) {
                if(pseudostate.getOwner().getOwner() instanceof State) { // if the owner of the owner of pseudostate is a State
                    stateOwningPseudostate = (State) pseudostate.getOwner().getOwner();
                    // ["pseudostateKind"_in_...]
                    sb.append("_in_").append(compositeName(stateOwningPseudostate, true));
                }
            }
        }
        if(!pseudostate.getIncomings().isEmpty()) { // if pseudostate has incoming transitions
            if(pseudostate.getIncomings().get(0).getSource() instanceof State) { // the first transition is from a state
                state = (State) pseudostate.getIncomings().get(0).getSource();
                // ["pseudostateKind"_in_..._from_"state"]
                sb.append("_from_").append(removePunctuation(removeSpace(state.getName())));
            }
            if(pseudostate.getIncomings().get(0).getSource() instanceof Pseudostate) { // the first transition is from a pseudostate
                otherPseudostate = (Pseudostate) pseudostate.getIncomings().get(0).getSource();
                // ["pseudostateKind"_in_..._from_...]
                sb.append("_from_").append(pseudoStateNameGenerator(otherPseudostate));
            }
            for(int i = 1; i < pseudostate.getIncomings().size(); i++) { // we do the same with the following incoming transitions
                if(pseudostate.getIncomings().get(i).getSource() instanceof State) {
                    state = (State) pseudostate.getIncomings().get(i).getSource();
                    // ["pseudostateKind"_in_..._from_..._and_"state"]
                    sb.append("_and_").append(removeSpace(state.getName()));
                }
                if(pseudostate.getIncomings().get(i).getSource() instanceof Pseudostate) {
                    otherPseudostate = (Pseudostate) pseudostate.getIncomings().get(i).getSource();
                    // ["pseudostateKind"_in_..._from_..._and_...]
                    sb.append("_and_").append(pseudoStateNameGenerator(otherPseudostate));
                }
            }
        }
        name = sb.toString();
        return name;
    }
    
    /**
     * Parses the state list in order to determine if composite names have to be applied or not
     * @param j 
     * @return 
     */
    private Boolean needCompositeName(int j) {
        // we compare every name of the list with each other
        for(int i = j+1; i < state_list.size(); i++) {
            if(state_list.get(j).getName().equals(state_list.get(i).getName()))
                return true;
        }
        if(j < state_list.size()-1) {
            return needCompositeName(j+1);
        } else {
            return false;
        }
    }
    
    /**
     * Determine if the generated class needs to implement the interface Manageable that implements in_state
     * @return boolean
     */
    private Boolean needImplementsManageable() {
        /* Init */
        Boolean b = false;
        
        /* Body */
        // if there is a pseudostate of kind "join" we need the interface Manageable in order to use in_state
        for(Pseudostate ps : pseudostate_list) {
            if(ps.getKind().toString().equals("join"))
                b = true;
        }
        return b;
    }
    
    /**
     * Used to print the parameters and guards of the event (fires).
     * @param t the concerned transition 
     */
    private void eventParameterAndGuards(Transition t) {
        
        /* Init */
        Constraint c; // the guard of the transition t
        PackageableElement p; // the package owning operations
        Parameter param;
        Boolean isConstraintAlreadyInInv_list, somethingWasNull;
        
        /* Body */
        if(!t.getOwnedRules().isEmpty()) { // if transition has a guard
            for(Element e : t.getOwnedRules()) {
                c = (Constraint)e;
                if(!inv_list.contains(c)) { // we update the list of invariant if needed
                    isConstraintAlreadyInInv_list = false;
                    for(int i=0; i<inv_list.size(); i++) {
                        // we check if the specification of the constraint doesn't correspond to another already existing
                        if(inv_list.get(i).getSpecification().stringValue().equals(c.getSpecification().stringValue()))
                            isConstraintAlreadyInInv_list = true;
                    }
                    if(!isConstraintAlreadyInInv_list)
                        inv_list.add(c);
                }
                write(SM_class,", bg, \"" + translateBoolean(c.getSpecification().stringValue())+"\"", true);
            }
        } else {
            if(t.getEffect() != null) // if the transition has an operation
                write(SM_class,", true",true);
        }
        if(t.getEffect() != null) {
            if(t.getEffect().getSpecification() != null) {
                if(!t.getEffect().getSpecification().getOwnedParameters().isEmpty()) { // if the transition has an operation with parameters
                    write(SM_class,", bo, \""+ removeSpace(t.getEffect().getSpecification().getName())+"\", new Object[]{", true);
                    p = (PackageableElement) t.getEffect().getSpecification().getOwner();
                    if(p.getName() != null && t.getEffect().getSpecification().getName() != null) { // we make sure the names are not empty otherwise the xmi file is wrong
                        // first parameter
                        param = t.getEffect().getSpecification().getOwnedParameters().get(0);
                        if(param.getType() != null && !param.getDirection().toString().equals("return")) {
                            write(SM_class,"new "+pickType(param.getType().toString())+"()",true);
                            // other parameters
                            for(int j = 1; j<t.getEffect().getSpecification().getOwnedParameters().size(); j++) { 
                                param = t.getEffect().getSpecification().getOwnedParameters().get(j);
                                if(param.getType() != null && !param.getDirection().toString().equals("return"))
                                    write(SM_class,", new "+pickType(param.getType().toString())+"()",true);
                            }
                        }
                    }
                    write(SM_class,"}", true);
                } else { // the transition has an operation with no parameter
                    p = (PackageableElement) t.getEffect().getSpecification().getOwner();
                    // we make sure the names are not empty otherwise the xmi file is wrong
                    if(p.getName() != null && t.getEffect().getSpecification().getName() != null) {
                        write(SM_class,", bo, \""+ removeSpace(t.getEffect().getSpecification().getName())+"\"", true);
                    }
                }
                // we update the hashmap because the operation has been used and has to be printed
                hm_operations.replace((Operation)t.getEffect().getSpecification(), true);
            }
        }
        write(SM_class,");\n",true);
        
    }
    
    /**
     * Print initial state.
     * @param state the concerned state
     */
    private void initialState(State state) {
        /* Init */
        Pseudostate ps;
        
        /* Body
        We gather the list of all incoming transitions of state 
        if the source of an incoming transition is a pseudostate and is kind of "initial"
        we have found an initial state */
        for(Transition transition : state.getIncomings()) {
            if(transition.getSource() instanceof Pseudostate) {
                ps = (Pseudostate) transition.getSource();
                if(ps.getKind().toString().equals("initial"))
                    write(SM_class, "\t\t"+compositeName(state, isNameComposite)+".inputState();\n", Boolean.TRUE);
            }
        }
    }
    
    /**
     * Print state invariant
     * @param state the concerned state
     */
    private void stateInvariant(State state) {
        /* Init */
        State constrainedState;
        
        /* Body 
        For each element of the list of invariant we look for their constrained elements
        if it corresponds to state we have found a stateInvariant */
        for(int i = 0; i < inv_list.size(); i++){
            for(Element e : inv_list.get(i).getConstrainedElements()) {
                constrainedState = (State)e;
                if(state.getName().equals(constrainedState.getName()) && inv_list.get(i).getSpecification() != null) {
                    write(SM_class, "\t\t"+compositeName(state, isNameComposite)+".stateInvariant(bg, \""+translateBoolean(inv_list.get(i).getSpecification().stringValue())+"\");\n", Boolean.TRUE);
                }
            }
        }
    }
    
    /**
     * Print deep and shallow history.
     * @param state the concerned state
     */
    private void deepAndShallowHistory(State state) {
        /* Init */
        Region r;
        Pseudostate ps;
        
        /* Body 
        If an element of the region of the state is a pseudostate
        and its kind is deepHistory or shallowHistory */
        for(Element e : state.getOwnedElements()) {
            if(e instanceof Region) {
                r = (Region)e;
                for(Element elt : r.getOwnedElements()) {
                    if(elt instanceof Pseudostate) {
                        ps = (Pseudostate)elt;
                        if(ps.getKind().toString().equals("deepHistory"))
                            write(SM_class,"\t\t"+compositeName(state, isNameComposite)+".deep_history();\n", true);
                        if(ps.getKind().toString().equals("shallowHistory"))
                            write(SM_class,"\t\t"+compositeName(state, isNameComposite)+".shallow_history();\n", true);
                    }
                }
            }
        }
    }
    
    /**
     * Print output state
     * @param state the concerned state
     */
    private void outputState(State state) {
        // if in the list of outgoing transitions the target is Finalstate
        state.getOutgoings().stream().filter((t) -> (t.getTarget() instanceof FinalState)).forEachOrdered((_item) -> {
            write(SM_class, "\t\t"+compositeName(state, isNameComposite)+".outputState();\n", Boolean.TRUE);
        });
    }
    
    /**
     * Print action.
     * @param state the concerned state
     * @param behavior the concerned behavior of the state
     * @param typeAction entry, exit or doActivity
     */
    private void action(State state, Behavior behavior, String typeAction) {
        /* Init */
        Parameter param;
        State s; // needed to deal with reentrance
        Boolean isReentrant = false;
        
        /* Body */
        if(behavior != null) {
            if(behavior.getSpecification() != null) {
                if(typeAction.equals("entry") && behavior.getSpecification().getName() != null)
                    write(SM_class, "\t\t"+compositeName(state, isNameComposite)+".set_entryAction(bo, \""+removeSpace(behavior.getSpecification().getName())+"\"", Boolean.TRUE);
                if(typeAction.equals("exit") && behavior.getSpecification().getName() != null)
                    write(SM_class, "\t\t"+compositeName(state, isNameComposite)+".set_exitAction(bo, \""+removeSpace(behavior.getSpecification().getName())+"\"", Boolean.TRUE);
                if(typeAction.equals("doActivity") && behavior.getSpecification().getName() != null)
                    write(SM_class, "\t\t"+compositeName(state, isNameComposite)+".doActivity(bo, \""+removeSpace(behavior.getSpecification().getName())+"\"", Boolean.TRUE);
                if(behavior.getSpecification().getOwnedParameters().isEmpty()) { // if the behaviour has no parameter
                    // Reentrance case
                    if(typeAction.equals("entry")) {
                        if(behavior.isReentrant()) {
                            write(SM_class, ", null, AbstractStatechart.Reentrance);\n", Boolean.TRUE);
                        }
                    } else {
                        write(SM_class, ");\n", Boolean.TRUE);
                    }
                } else { // if the bahaviour has parameter(s)
                    param = behavior.getSpecification().getOwnedParameters().get(0);
                    if(!param.getDirection().toString().equals("return")) {
                        if(param.getType() != null) {
                            // first parameter
                            write(SM_class, ", new Object[]{new "+ pickType(param.getType().toString())+"()", Boolean.TRUE);
                            // other parameters
                            for(int j = 1; j < behavior.getSpecification().getOwnedParameters().size(); j++) {
                                param = behavior.getSpecification().getOwnedParameters().get(j);
                                if(!param.getDirection().toString().equals("return"))
                                    if(param.getType() != null)
                                        write(SM_class,", new "+pickType(param.getType().toString())+"()",true);
                            }
                            write(SM_class, "}", Boolean.TRUE);
                            // reentrance case
                            if(typeAction.equals("entry")) {
                                if(behavior.isSetIsReentrant()) { // if the reentrance feature of the behavior has been set
                                    if(behavior.isReentrant()) // if the reentrance feature has been set on true
                                        write(SM_class, ", AbstractStatechart.Reentrance", Boolean.TRUE);
                                } else { // it has not been set
                                    s = (State) behavior.getOwner();
                                    for(Transition transition : s.getIncomings()) {
                                        // if one of the transition of the state has this state as source and target then the behavior is reentrant
                                        if(transition.getSource() == transition.getTarget()) {
                                            isReentrant = true;
                                        }
                                    }
                                    if(isReentrant)
                                        write(SM_class, ", AbstractStatechart.Reentrance", Boolean.TRUE);
                                }
                            }
                        }
                    }
                    write(SM_class, ");\n", Boolean.TRUE);
                }
                hm_operations.replace((Operation)behavior.getSpecification(), true);
            }
        }
    }        
    
    /**
     * Print allowed event.
     * @param state the concerned state
     */
    private void allowedEvent(State state) {
        /* Init */
        Parameter param;
        Boolean somethingWasNull = true;
        
        /* Body */
        /*** 1ST STEP: we check if something is null and might cause an error ***/
        // MARCHE PAS
        for(Transition t : state.getOutgoings()) {
            if(t.getSource().equals(t.getTarget()) && t.getTarget().equals(state)) { // if the source and the target of the transition are state
                if(t.getKind().toString().equals("internal")) { // if the kind of the transition is internal
                    if(t.getTriggers() != null) { // if the transition has events
                        for(Trigger trigger : t.getTriggers()) {
                            if(trigger.getEvent() != null) {
                                if(t.getEffect() != null) {
                                    if(state.getName() != null && trigger.getEvent().getName() != null) {
                                        // if the transition has a guard
                                        if(t.getGuard() != null) {
                                            for(Constraint c : inv_list) {
                                                somethingWasNull = !(c.equals(t.getGuard()) && c.getSpecification() != null && somethingWasNull);
                                            }
                                        }
                                        // if the transition has an operation
                                        if(t.getEffect().getSpecification() != null) {
                                            if(t.getEffect().getSpecification().getOwner() != null) {
                                                if(t.getEffect().getSpecification().getName() != null) {
                                                    if(t.getEffect().getSpecification().getOwnedParameters().isEmpty()) { // if the operation has no parameter
                                                        somethingWasNull = false;
                                                    } else { // if the operation has parameters
                                                        param = t.getEffect().getSpecification().getOwnedParameters().get(0);
                                                        if(param.getType() != null) {
                                                            for(int i = 1; i < t.getEffect().getSpecification().getOwnedParameters().size(); i++) {
                                                                param = t.getEffect().getSpecification().getOwnedParameters().get(i);
                                                                somethingWasNull = !(param.getType() != null && somethingWasNull);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        /*** 2ND STEP: we print if nothing was null ***/
        if(!somethingWasNull) {
            for(Transition t : state.getOutgoings()) {
                if(t.getSource().equals(t.getTarget()) && t.getTarget().equals(state)) { // if the source and the target of the transition are state
                    if(t.getKind().toString().equals("internal")) { // if the kind of the transition is internal
                        for(Trigger trigger : t.getTriggers()) { // for each event of the transition
                            write(SM_class, "\t\t"+state.getName()+".allowedEvent(\""+trigger.getEvent().getName()+"\",", Boolean.TRUE);
                            // GUARD
                            if(t.getGuard() != null) {
                                // for each constraint c from the inv_list that corresponds to the guard of t
                                inv_list.stream().filter((c) -> (c.equals(t.getGuard()))).forEachOrdered((c) -> {
                                    write(SM_class, " bg, \'"+translateBoolean(c.getSpecification().stringValue())+"\",", true);
                                });
                            } else {
                                write(SM_class, " this, true,", Boolean.TRUE);
                            }
                            // OPERATION
                            write(SM_class, "bo, \""+removeSpace(t.getEffect().getSpecification().getName())+"\"", Boolean.TRUE);
                            if(t.getEffect().getSpecification().getOwnedParameters().isEmpty()) { // if there is no parameter
                                write(SM_class, ");\n", Boolean.TRUE);
                            } else { // if there are parameters
                                param = t.getEffect().getSpecification().getOwnedParameters().get(0);
                                write(SM_class, ", new Object[]{ new "+ pickType(param.getType().toString())+"()", Boolean.TRUE);
                                for(int i = 1; i < t.getEffect().getSpecification().getOwnedParameters().size(); i++) {
                                    param = t.getEffect().getSpecification().getOwnedParameters().get(i);
                                    write(SM_class,", new "+pickType(param.getType().toString())+"()",true);
                                }
                                write(SM_class, "});\n", Boolean.TRUE);
                            }
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Generate empty transition
     * @param state the concerned state
     */
    public void setEntry_EmptyTransition(State state) {
        // when a transition from state has no trigger and is not heading to a final state it is an empty transition
        if(!state.getOutgoings().isEmpty()) {
            for(int i = 0; i < state.getOutgoings().size(); i++) {
                if(state.getOutgoings().get(i).getTriggers().isEmpty() && !(state.getOutgoings().get(i).getTarget() instanceof FinalState))
                    write(SM_class, "\t\t"+compositeName(state, isNameComposite)+".set_entryAction(this, \"emptyTransition_"+compositeName(state, isNameComposite)+"\", null, AbstractStatechart.Reentrance);\n", Boolean.TRUE);
            }
        }
    }
    
    /**
     * Print the fires from state
     * @param transition the concerned transition
     */
    public void firesFromState(Transition transition) {
        /* Init */
        Pseudostate ps;
        State otherState;
        
        /* Body */
        /************************************************************
         *                 start() :: STATE -> STATE
         ***********************************************************/
        if(transition.getTarget() instanceof State) {
            if(transition.getTriggers().isEmpty()) { 
                /* EMPTY TRANSITION */
                write(SM_class,"\t\t"+SM_name+".fires(\"emptyTransition_"+compositeName((State)transition.getSource(), isNameComposite)+"\", "+compositeName((State)transition.getSource(), isNameComposite)+", "+compositeName((State)transition.getTarget(), isNameComposite),true);
                eventParameterAndGuards(transition);
            } else {
                /* TRANSITION WITH TRIGGER */
                for(Trigger trigger : transition.getTriggers()) {
                    if(trigger.getEvent() != null) {
                        write(SM_class,"\t\t"+SM_name+".fires(\""+removeSpace(trigger.getEvent().getName())+"\", "+compositeName((State)transition.getSource(), isNameComposite)+", "+compositeName((State)transition.getTarget(), isNameComposite),true);
                        eventParameterAndGuards(transition);
                    }
                }
            }
        }
        /************************************************************
         *              start() :: STATE -> PSEUDOSTATE
         ***********************************************************/
        if(transition.getTarget() instanceof Pseudostate) { // if transition is heading a Pseudostate (entry point, exit point, deepHistory, shallowHistory, initial, join, fork, junction, choice)
            ps = (Pseudostate) transition.getTarget();
            if(transition.getTriggers().isEmpty()) {
                /* EMPTY TRANSITION */
                if(ps.getKind().toString().equals("deepHistory") || transition.getKind().toString().equals("shallowHistory")) {
                    if(ps.getOwner().getOwner() != null) {
                        if(ps.getOwner().getOwner() instanceof State) {
                            otherState = (State) ps.getOwner().getOwner();
                            write(SM_class,"\t\t"+SM_name+".fires(\"emptyTransition_"+compositeName((State)transition.getSource(), isNameComposite)+"\", "+compositeName((State)transition.getSource(), isNameComposite)+", "+compositeName(otherState, isNameComposite),true);
                            eventParameterAndGuards(transition);
                        }
                    }
                }
                if(ps.getKind().toString().equals("fork") || ps.getKind().toString().equals("junction") || ps.getKind().toString().equals("choice") || ps.getKind().toString().equals("terminate")) {
                    write(SM_class,"\t\t"+SM_name+".fires(\"emptyTransition_"+compositeName((State)transition.getSource(), isNameComposite)+"\", "+compositeName((State)transition.getSource(), isNameComposite)+", "+pseudoStateNameGenerator(ps),true);
                    eventParameterAndGuards(transition);
                }
                if(ps.getKind().toString().equals("join")) {
                    for(int i=0; i < ps.getIncomings().size(); i++) {
                        if(ps.getIncomings().get(i).equals(transition)) {
                            if(i == 0) {
                                write(SM_class,"\t\t"+SM_name+".fires(\"emptyTransition_"+compositeName((State)transition.getSource(), isNameComposite)+"\", "+compositeName((State)transition.getSource(), isNameComposite)+", "+pseudoStateNameGenerator(ps)+", this, \"guard_"+ pseudoStateNameGenerator(ps)+"\");\n",true);
                            }
                        }
                    }
                }
            } else {
                /* TRANSITION WITH TRIGGER */
                for(Trigger trigger : transition.getTriggers()) {
                    if(trigger.getEvent() != null) {
                        if(ps.getKind().toString().equals("deepHistory") || transition.getKind().toString().equals("shallowHistory")) {
                            if(ps.getOwner().getOwner() != null) {
                                if(ps.getOwner().getOwner() instanceof State) {
                                    otherState = (State) ps.getOwner().getOwner();
                                    if(trigger.getEvent().getName() != null) {
                                        write(SM_class,"\t\t"+SM_name+".fires(\""+removeSpace(trigger.getEvent().getName())+"\", "+compositeName((State)transition.getSource(), isNameComposite)+", "+compositeName(otherState, isNameComposite),true);
                                        eventParameterAndGuards(transition);
                                    }
                                }
                            }
                        }
                        if(ps.getKind().toString().equals("fork") || ps.getKind().toString().equals("junction") || ps.getKind().toString().equals("choice") || ps.getKind().toString().equals("terminate")) {
                            if(trigger.getEvent().getName() != null) {
                                write(SM_class,"\t\t"+SM_name+".fires(\""+removeSpace(trigger.getEvent().getName())+"\", "+compositeName((State)transition.getSource(), isNameComposite)+", "+pseudoStateNameGenerator(ps),true);
                                eventParameterAndGuards(transition);
                            }
                        }
                        if(ps.getKind().toString().equals("join")) {
                            for(int i=0; i < ps.getIncomings().size(); i++) {
                                if(ps.getIncomings().get(i).equals(transition)) {
                                    if(i == 0 && trigger.getEvent().getName() != null) {
                                        write(SM_class,"\t\t"+SM_name+".fires(\""+removeSpace(trigger.getEvent().getName())+"\", "+compositeName((State)transition.getSource(), isNameComposite)+", "+pseudoStateNameGenerator(ps)+", bg, \"guard_"+ pseudoStateNameGenerator(ps)+"\");\n",true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Print the fires from pseudostate
     * @param transition the concerned transition
     */
    private void firesFromPseudostate(Transition transition) {
        /* Init */
        Pseudostate ps, otherPseudostate;
        int nbOfInitialState = 0;
        State s, state = null;
        
        /* Body */
        ps = (Pseudostate) transition.getSource();
        if(!ps.getOutgoings().isEmpty()) {
            if(transition.getTarget() instanceof State && !(transition.getTarget() instanceof FinalState)) { // if the transition is heading a state
                /* JUNCTION - JOIN - CHOICE */
                if(ps.getKind().toString().equals("junction") || ps.getKind().toString().equals("join") || ps.getKind().toString().equals("choice")) {
                    write(SM_class,"\t\t"+SM_name+".fires(\""+pseudoStateNameGenerator(ps)+"\", "+pseudoStateNameGenerator(ps)+", "+compositeName((State)transition.getTarget(), isNameComposite),true);
                    eventParameterAndGuards(transition);
                }
                /* FORK */
                if(ps.getKind().toString().equals("fork")) {
                    if(transition.equals(ps.getOutgoings().get(0))) {
                        // if the transition is coming FROM the fork
                        if(ps.getOutgoings().size() == 1) {
                            write(SM_class,"\t\t"+SM_name+".fires(\""+pseudoStateNameGenerator(ps)+"\", "+pseudoStateNameGenerator(ps)+", "+compositeName((State)transition.getTarget(), isNameComposite),true);
                            eventParameterAndGuards(transition);
                        } else { 
                            // if the transition is going TO the fork
                            for(Transition trans : ps.getOutgoings()) {
                                if(trans.getTarget() instanceof State) { // the generator does not support the transition from Fork to another pseudostate
                                    s = (State) trans.getTarget();
                                    if(!s.getIncomings().isEmpty()) {
                                        /* Fork can deal with two scenario: 
                                        -1- every transition is pointing a state with an initial state
                                        -2- every transition is pointing a state with an initial state except one
                                        Note that if we are in a situation where neither one nor the other case is reached we do not process it.
                                        */
                                        for(Transition otherTrans : s.getIncomings()) {
                                            if(otherTrans.getSource() instanceof Pseudostate) {
                                                otherPseudostate = (Pseudostate) otherTrans.getSource();
                                                if(otherPseudostate.getKind().toString().equals("initial"))
                                                    nbOfInitialState++;
                                            } else {
                                                state = (State) otherTrans.getSource();
                                            }
                                        }
                                    }
                                }
                            }
                            if(nbOfInitialState == ps.getOutgoings().size()) {
                                // case -1-
                                write(SM_class,"\t\t"+SM_name+".fires(\""+pseudoStateNameGenerator(ps)+"\", "+pseudoStateNameGenerator(ps)+", "+compositeName((State)transition.getTarget().getOwner().getOwner(), isNameComposite),true);
                                eventParameterAndGuards(transition);
                            } else if (nbOfInitialState == ps.getOutgoings().size()-1) {
                                // case -2-
                                write(SM_class,"\t\t"+SM_name+".fires(\""+pseudoStateNameGenerator(ps)+"\", "+pseudoStateNameGenerator(ps)+", "+compositeName(state, isNameComposite),true);
                                eventParameterAndGuards(transition);
                            }
                        }
                    }
                }
            } else { // if the transition is heading a pseudostate
                /* JUNCTION - JOIN - CHOICE */
                if(ps.getKind().toString().equals("junction") || ps.getKind().toString().equals("join") || ps.getKind().toString().equals("choice")) {
                    write(SM_class,"\t\t"+SM_name+".fires(\""+pseudoStateNameGenerator(ps)+"\", "+pseudoStateNameGenerator(ps)+", "+pseudoStateNameGenerator((Pseudostate) transition.getTarget()),true);
                    eventParameterAndGuards(transition);
                }
            }
        }
    }
    
    /**
     * Print event
     * @param event the concerned event
     */
    private void event(String event) {
        /* Init */
        Boolean eventHasBeenFound = false;
        Constraint c;
        Parameter param;
        Boolean somethingWasNull = true;
        Boolean guardWasNull = true;
        Boolean firstParameterIsWrong = false;
        
        /* Body */
        for(Transition transition : trans_list) {
            if(!eventHasBeenFound) {
                for(Trigger trigger : transition.getTriggers()) {
                    if(trigger.getEvent() != null || trigger.getName() != null) {
                        if(trigger.getEvent().getName() != null)
                            // if the source of the transition is a state and trigger correspond to event and event has not been found yet
                            if(transition.getSource() instanceof State && trigger.getEvent().getName().equals(event) && !eventHasBeenFound)
                                eventHasBeenFound = true;
                        // same as above
                        if(transition.getSource() instanceof State && trigger.getName().equals(event) && !eventHasBeenFound)
                            eventHasBeenFound = true;
                        if(eventHasBeenFound) {
                            /* HEAD OF GENERATED EVENT METHOD */
                            write(SM_class, "\tpublic void "+removeSpace(event)+"(", true);
                            if(transition.getEffect() != null) {
                                if(transition.getEffect().getSpecification() != null) {
                                    // if the opration has parameters
                                    if(!transition.getEffect().getSpecification().getOwnedParameters().isEmpty()) {
                                        // first parameter
                                        param = transition.getEffect().getSpecification().getOwnedParameters().get(0);
                                        if(param.getType() != null && param.getName() != null && !param.getDirection().toString().equals("return"))
                                            write(SM_class,pickType(param.getType().toString())+" "+removeSpace(param.getName()),true);
                                        else 
                                            firstParameterIsWrong = true;
                                        // other parameters
                                        for(int j = 1; j<transition.getEffect().getSpecification().getOwnedParameters().size(); j++) {
                                            param = transition.getEffect().getSpecification().getOwnedParameters().get(j);
                                            if(param.getType() != null && param.getName() != null && !param.getDirection().toString().equals("return")) {
                                                if(firstParameterIsWrong) {
                                                    firstParameterIsWrong = false;
                                                    write(SM_class,pickType(param.getType().toString())+" "+removeSpace(param.getName()),true);
                                                } else {
                                                    write(SM_class,", "+pickType(param.getType().toString())+" "+removeSpace(param.getName()),true);
                                                }
                                            }
                                        }
                                    }
                                    write(SM_class, ") throws Statechart_exception {\n", true);
                                    /* INSIDE THE GENERATED EVENT METHOD */
                                    if(!transition.getEffect().getSpecification().getOwnedParameters().isEmpty()) {
                                        for(Transition transitionForFires : trans_list) {
                                            for(Trigger triggerForFires : transitionForFires.getTriggers()) {
                                                if(triggerForFires.getEvent() != null) {
                                                    if(triggerForFires.getEvent().getName() != null) {
                                                        somethingWasNull = false;
                                                        if(triggerForFires.getEvent().getName().equals(event)) {
                                                            write(SM_class,"\t\t"+SM_name+".fires(\""+removeSpace(event)+"\", "+removeSpace(transitionForFires.getSource().getName())+", "+removeSpace(transitionForFires.getTarget().getName()), true);
                                                            if(!transitionForFires.getOwnedRules().isEmpty()) { // if there is a guard
                                                                for(Element e : transitionForFires.getOwnedRules()) {
                                                                    c = (Constraint)e;
                                                                    if(c.getSpecification() != null) {
                                                                        write(SM_class,", bg, \"" + translateBoolean(c.getSpecification().stringValue())+"\"", true);
                                                                        guardWasNull = false;
                                                                    }
                                                                }
                                                            } 
                                                            if(guardWasNull) { // if there is no guard
                                                                write(SM_class,", true",true);
                                                            }
                                                            if(transitionForFires.getEffect() != null) {
                                                                if(transition.getEffect().getSpecification() != null) {
                                                                    if(!transitionForFires.getEffect().getSpecification().getOwnedParameters().isEmpty()) { // if there are parameters
                                                                        write(SM_class,", bo, \""+ removeSpace(transitionForFires.getEffect().getSpecification().getName())+"\", new Object[]{", true);
                                                                        // first parameter
                                                                        param = transitionForFires.getEffect().getSpecification().getOwnedParameters().get(0);
                                                                        if(!param.getDirection().toString().equals("return") && param.getName() != null) {
                                                                            write(SM_class,param.getName(),true);
                                                                            // other parameters
                                                                            for(int j = 1; j<transitionForFires.getEffect().getSpecification().getOwnedParameters().size(); j++) {
                                                                                param = transitionForFires.getEffect().getSpecification().getOwnedParameters().get(j);
                                                                                if(!param.getDirection().toString().equals("return") && param.getName() != null) {
                                                                                    write(SM_class, ", " + param.getName(), true);
                                                                                }
                                                                            }
                                                                        }
                                                                        write(SM_class,"}", true);
                                                                    } else { // if there is no parameter
                                                                        if(transitionForFires.getEffect().getSpecification().getName() != null)
                                                                            write(SM_class,", bo, \""+ removeSpace(transitionForFires.getEffect().getSpecification().getName())+"\"", true);
                                                                    }
                                                                }
                                                            }
                                                            write(SM_class,");\n", true);
                                                        }
                                                    }
                                                }
                                                if(triggerForFires.getName() != null && somethingWasNull) {
                                                    if(triggerForFires.getName().equals(event)) {
                                                        write(SM_class,"\t\t"+SM_name+".fires(\""+removeSpace(event)+"\", "+removeSpace(transitionForFires.getSource().getName())+", "+removeSpace(transitionForFires.getTarget().getName()), true);
                                                        if(!transitionForFires.getOwnedRules().isEmpty()) { // if there is a guard
                                                            for(Element e : transitionForFires.getOwnedRules()) {
                                                                c = (Constraint)e;
                                                                if(c.getSpecification() != null) {
                                                                    write(SM_class,", bg, \"" + translateBoolean(c.getSpecification().stringValue())+"\"", true);
                                                                    guardWasNull = false;
                                                                }
                                                            }
                                                        } 
                                                        if(guardWasNull) { // if there is no guard
                                                            write(SM_class,", true",true);
                                                        }
                                                        if(transitionForFires.getEffect() != null) {
                                                            if(transition.getEffect().getSpecification() != null) {
                                                                if(!transitionForFires.getEffect().getSpecification().getOwnedParameters().isEmpty()) { // if there are parameters
                                                                    write(SM_class,", bo, \""+ removeSpace(transitionForFires.getEffect().getSpecification().getName())+"\", new Object[]{", true);
                                                                    // first parameter
                                                                    param = transitionForFires.getEffect().getSpecification().getOwnedParameters().get(0);
                                                                    write(SM_class,"new "+pickType(param.getType().toString())+"()",true);
                                                                    // other parameters
                                                                    for(int j = 1; j<transitionForFires.getEffect().getSpecification().getOwnedParameters().size(); j++) {
                                                                        param = transitionForFires.getEffect().getSpecification().getOwnedParameters().get(j);
                                                                        write(SM_class,", new "+pickType(param.getType().toString())+"()",true);
                                                                    }
                                                                    write(SM_class,"}", true);
                                                                } else { // if there is no parameter
                                                                    if(transitionForFires.getEffect().getSpecification().getName() != null)
                                                                        write(SM_class,", bo, \""+ removeSpace(transitionForFires.getEffect().getSpecification().getName())+"\"", true);
                                                                }
                                                            }
                                                        }
                                                        write(SM_class,");\n", true);
                                                    }
                                                }                                                
                                            }
                                        }
                                    }
                                }
                            } else {
                                write(SM_class, ") throws Statechart_exception {\n", true);
                            }
                            write(SM_class, "\t\t"+SM_name+".run_to_completion(\""+removeSpace(trigger.getEvent().getName())+"\"", true);
                            if(transition.getGuard()!=null)
                                    write(SM_class,", AbstractStatechart_monitor.Compute_invariants", true);
                            write(SM_class,");\n\t}\n\n", true);
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Build composite states and paralel states
     * @param state 
     */
    private void compositeStates(State state) {
        /* Init */
        State stateInsideCompositeState; // state inside a composite state
        Region r;
        Pseudostate ps;
        List<State> list_state; // list of states contained by a composite state
        List<Vertex> list_vertex; // list of vertices (states + pseudostates) contained by a composite state
        HashMap<Integer, List<Vertex>> hm_region_vertices; // hashmap of regions of a potential paralel composite state and its corresponding list of vertices
        int countRegion = 0; // used for the keys of hm_region_vertices
        List<Vertex> tmp; //used for the value of hm_region_vertices
        
        /* Body */
        list_state = new ArrayList();
        list_vertex = new ArrayList();
        hm_region_vertices = new HashMap();
        /* we gather the list of all states and vertices */
        for(Element e : state.getOwnedElements()){
            if(e instanceof Region) {
                r = (Region)e;
                tmp = new ArrayList();
                for(Element elt : r.getOwnedElements()) {
                    if(elt instanceof State && !(elt instanceof FinalState)) {
                        stateInsideCompositeState = (State)elt;
                        list_state.add(stateInsideCompositeState);
                        list_vertex.add(stateInsideCompositeState);
                        tmp.add(stateInsideCompositeState);
                    }
                    if(elt instanceof Pseudostate) {
                        ps = (Pseudostate)elt;
                        // we are only intersted in fork, choice, join and junction
                        if(!ps.getKind().toString().equals("initial") && !ps.getKind().toString().equals("final") && !ps.getKind().toString().equals("deepHistory") && !ps.getKind().toString().equals("shallowHistory") && !ps.getKind().toString().equals("terminate")) {
                            list_vertex.add(ps);
                            tmp.add(ps);
                        }
                    }
                }
                // for each region we give the list of vertices that are inside
                hm_region_vertices.put(countRegion, tmp);
                countRegion++;
            }
        }
        if(!state.isOrthogonal()) { // if the composite state is not paralel
            if(list_vertex.size() == 1) { // if the composite state has only one vertex
                write(SM_class, compositeName((State)list_vertex.get(0), isNameComposite)+".and(new ", Boolean.TRUE);
                if(!EEorME) // if user has chosen Java ME version
                    write(SM_class, "Java_ME", Boolean.TRUE);
                write(SM_class,"Statechart(AbstractStatechart.Pseudo_state))",true);
            } else { // if the state has more than one vertices
                for(int i = 0; i<list_vertex.size(); i++) {
                    if(i == (list_vertex.size()-1)) { // if it is the last element of the composite state
                        if(list_vertex.get(i) instanceof State) {
                            write(SM_class, compositeName((State)list_vertex.get(i), isNameComposite), Boolean.TRUE);
                        } else {
                            write(SM_class, pseudoStateNameGenerator((Pseudostate)list_vertex.get(i)), Boolean.TRUE);
                        }
                        for(int j = 0; j < list_vertex.size();j++) {
                            write(SM_class, ")", Boolean.TRUE);
                        }
                    } else { // if there is still more than one element to deal with inside the composite state
                        if(list_vertex.get(i) instanceof State) {
                            write(SM_class, compositeName((State)list_vertex.get(i), isNameComposite)+".xor(", Boolean.TRUE);
                        } else {
                            write(SM_class, pseudoStateNameGenerator((Pseudostate)list_vertex.get(i))+".xor(", Boolean.TRUE);
                        }
                    }
                }
            }
        } else { // if the composite state is paralel
            // we open one bracket per region
            for(int i = 1; i < hm_region_vertices.size(); i++) {
                write(SM_class,"(",true);
            }
            /* FIRST REGION */
            for(int i = 0; i < hm_region_vertices.get(0).size(); i++) {
                if(i == (hm_region_vertices.get(0).size()-1)) { // if it is the last element of the composite state
                    if(hm_region_vertices.get(0).get(i) instanceof State) {
                        write(SM_class, compositeName((State)hm_region_vertices.get(0).get(i), isNameComposite), Boolean.TRUE);
                    } else {
                        write(SM_class, pseudoStateNameGenerator((Pseudostate)hm_region_vertices.get(0).get(i)), Boolean.TRUE);
                    }
                    for(int j = 0; j < hm_region_vertices.get(0).size();j++) {
                        write(SM_class, ")", Boolean.TRUE);
                    }
                } else { // if there is still more than one element to deal with inside the composite state
                    if(hm_region_vertices.get(0).get(i) instanceof State) {
                        write(SM_class, compositeName((State)hm_region_vertices.get(0).get(i), isNameComposite)+".xor(", Boolean.TRUE);
                    } else {
                        write(SM_class, pseudoStateNameGenerator((Pseudostate)hm_region_vertices.get(0).get(i))+".xor(", Boolean.TRUE);
                    }
                }
            }
            write(SM_class,".and(",true);
            /* OTHER REGION EXCEPT LAST ONE */
            for(int i = 1; i < hm_region_vertices.size()-1; i++) {
                for(int j = 0; j < hm_region_vertices.get(i).size(); j++) {
                    if(j == (hm_region_vertices.get(i).size()-1)) { // if it is the last element of the composite state
                        if(hm_region_vertices.get(i).get(j) instanceof State) {
                            write(SM_class, compositeName((State)hm_region_vertices.get(i).get(j), isNameComposite), Boolean.TRUE);
                        } else {
                            write(SM_class, pseudoStateNameGenerator((Pseudostate)hm_region_vertices.get(i).get(j)), Boolean.TRUE);
                        }
                        for(int k = 0; k < hm_region_vertices.get(i).size();k++) {
                            write(SM_class, ")", Boolean.TRUE);
                        }
                    } else { // if there is still more than one element to deal with inside the composite state
                        if(hm_region_vertices.get(i).get(j) instanceof State) {
                            write(SM_class, compositeName((State)hm_region_vertices.get(i).get(j), isNameComposite)+".xor(", Boolean.TRUE);
                        } else {
                            write(SM_class, pseudoStateNameGenerator((Pseudostate)hm_region_vertices.get(i).get(j))+".xor(", Boolean.TRUE);
                        }
                    }
                }
                write(SM_class,").and(",true);
            }
            /* LAST REGION */
            for(int i = 0; i < hm_region_vertices.get(hm_region_vertices.size()-1).size(); i++) {
                if(i == (hm_region_vertices.get(hm_region_vertices.size()-1).size()-1)) { // if it is the last element of the composite state
                    if(hm_region_vertices.get(hm_region_vertices.size()-1).get(i) instanceof State) {
                        write(SM_class, compositeName((State)hm_region_vertices.get(hm_region_vertices.size()-1).get(i), isNameComposite), Boolean.TRUE);
                    } else {
                        write(SM_class, pseudoStateNameGenerator((Pseudostate)hm_region_vertices.get(hm_region_vertices.size()-1).get(i)), Boolean.TRUE);
                    }
                    for(int j = 0; j < hm_region_vertices.get(hm_region_vertices.size()-1).size();j++) {
                        write(SM_class, ")", Boolean.TRUE);
                    }
                } else { // if there is still more than one element to deal with inside the composite state
                    if(hm_region_vertices.get(hm_region_vertices.size()-1).get(i) instanceof State) {
                        write(SM_class, compositeName((State)hm_region_vertices.get(hm_region_vertices.size()-1).get(i), isNameComposite)+".xor(", Boolean.TRUE);
                    } else {
                        write(SM_class, pseudoStateNameGenerator((Pseudostate)hm_region_vertices.get(hm_region_vertices.size()-1).get(i))+".xor(", Boolean.TRUE);
                    }
                }
            }
            write(SM_class,")",true);
        }
    }
    
    /**
     * This method call all the print functions and also prints stuff that are not enclosed in a method.
     */
    private void print() {
        
        /* Init */
        PackageableElement p;
        HashMap<Integer,State> hmOfCompositeState = new HashMap<>(); // contains the list of composite states
        Parameter param;
        List<String> list_interface = new ArrayList(); // list of all interface name
        List<String> list_event = new ArrayList();
        StringBuilder sb = new StringBuilder();
        Boolean hasReturnValue; // needed to know if a method has a return value or not
        Iterator it;
        Operation operation;
        
        /* Body */
        isNameComposite = needCompositeName(0);
        isManageableNeeded = needImplementsManageable();
        /************************************************************
         *                     Introduction
         ***********************************************************/
        write(SM_class, "import com.pauware.pauware_engine._Core.*;\nimport com.pauware.pauware_engine._Exception.*;\nimport com.pauware.pauware_engine._Java_", false);
        if(EEorME) { // if user has chosen Java EE or Java ME version
            write(SM_class, "EE.*;\n", true);
        } else {
            write(SM_class, "ME.*;\n", true);
        }
        write(SM_class, "\npublic class " + SM_class, true);
        if(timer) // if user has chosen to add timer notion
            write(SM_class, " extends Timer_monitor", true);
        if(isManageableNeeded)
            write(SM_class, " implements Manageable", true);
        write(SM_class, " {\n", true);
        /************************************************************
         *              State Machine declaration
         ***********************************************************/
        write(SM_class, "\n\t// State Machine\n\tprotected AbstractStatechart_monitor "+SM_name+";\n", true);
        /************************************************************
         *                  States declaration
         ***********************************************************/
        write(SM_class, "\n\t// States\n", true);
        state_list.forEach((s) -> {
            write(SM_class, "\tprotected AbstractStatechart "+compositeName(s, isNameComposite)+";\n", true);
        });
        pseudostate_list.forEach((pseudostate) -> {
            write(SM_class, "\tprotected AbstractStatechart "+pseudoStateNameGenerator(pseudostate)+";\n", true);
        });
        /************************************************************
         *            Business Objects declaration
         ***********************************************************/
        if(businessObject)
            write(SM_class,"\n\t// Business object associated with the SM\n\tprotected BusinessObject bo;\n", true);
        /************************************************************
         *            Business Guard declaration
         ***********************************************************/
        if(businessGuard)
            write(SM_class,"\n\tprotected BusinessGuard bg;\n", true);
        /************************************************************
         *                    initialize_SM()
         ***********************************************************/
        write(SM_class, "\n\tprivate void initialize_SM() throws Statechart_exception {\n", Boolean.TRUE);
        /************************************************************
         *     initialize_SM() :: businessObject initialisation
         ***********************************************************/
        if(businessObject)
            write(SM_class, "\t\tbo = new BusinessObject();\n", Boolean.TRUE);
        /************************************************************
         *     initialize_SM() :: businessGuard initialisation
         ***********************************************************/
        if(businessGuard)
            write(SM_class, "\t\tbg = new BusinessGuard();\n", Boolean.TRUE);
        /************************************************************
         *         initialize_SM() :: STATES 
         ***********************************************************/
        state_list.forEach((s) -> {
            if(s.isComposite()){
                hmOfCompositeState.put(counterOfCompositeState, s);
                counterOfCompositeState++;
            } else {
                /************************************************************
                 *         initialize_SM() :: states initialisation
                 ***********************************************************/
                write(SM_class, "\t\t"+compositeName(s, isNameComposite)+" = new ", Boolean.TRUE);
                if(!EEorME) // if user has chosen Java ME version
                    write(SM_class, "Java_ME", Boolean.TRUE);
                write(SM_class, "Statechart(\""+compositeName(s, isNameComposite)+"\");\n", Boolean.TRUE);
                /************************************************************
                 *          initialize_SM() :: Initial state
                 ***********************************************************/
                initialState(s);
                /************************************************************
                 *          initialize_SM() :: state invariant
                 ***********************************************************/
                stateInvariant(s);
                /************************************************************
                 *    initialize_SM() :: Deep & Shallow history
                 ***********************************************************/
                deepAndShallowHistory(s);
                /************************************************************
                 *              initialize_SM() :: output state 
                 ***********************************************************/
                outputState(s);
                /************************************************************
                 *              initialize_SM() :: allowedEvent 
                 ***********************************************************/
                allowedEvent(s);
                /************************************************************
                 *              initialize_SM() :: set_ENTRY 
                 ***********************************************************/
                action(s, s.getEntry(), "entry");
                /************************************************************
                 *              initialize_SM() :: set_EXIT 
                 ***********************************************************/
                action(s, s.getExit(), "exit");
                /************************************************************
                 *              initialize_SM() :: doACTIVITY 
                 ***********************************************************/
                action(s, s.getDoActivity(), "doActivity");
                /************************************************************
                 *      initialize_SM() :: set_ENTRY emptyTransition
                 ***********************************************************/
                setEntry_EmptyTransition(s);
            }
        });
        for (Pseudostate pseudostate : pseudostate_list) {
            /************************************************************
             *  initialize_SM() :: pseudostate fake state initialisation
             ***********************************************************/
            write(SM_class, "\t\t"+pseudoStateNameGenerator(pseudostate)+" = new ", Boolean.TRUE);
            if(!EEorME) // if user has chosen Java ME version
                write(SM_class, "Java_ME", Boolean.TRUE);
            write(SM_class, "Statechart(\""+pseudoStateNameGenerator(pseudostate)+"\");\n", Boolean.TRUE);
            /************************************************************
             *      initialize_SM() :: pseudostate set_ENTRY
             ***********************************************************/
            if(pseudostate.getKind().toString().equals("choice") || pseudostate.getKind().toString().equals("junction") || pseudostate.getKind().toString().equals("terminate") || pseudostate.getKind().toString().equals("fork") || pseudostate.getKind().toString().equals("join"))
                write(SM_class, "\t\t"+pseudoStateNameGenerator(pseudostate)+".set_entryAction(this, \""+pseudoStateNameGenerator(pseudostate)+"\", null, AbstractStatechart.Reentrance);\n", Boolean.TRUE);
        }
        /************************************************************
         *        initialize_SM() :: Composite States
         ***********************************************************/
        for(int i = counterOfCompositeState; i>=0; i--) {
            if(hmOfCompositeState.get(i) != null && hmOfCompositeState.get(i).isComposite()) {
                /************************************************************
                 *    initialize_SM() :: Composite states initialization
                 ***********************************************************/
                write(SM_class,"\t\t"+compositeName(hmOfCompositeState.get(i), isNameComposite)+" = (",true);
                compositeStates(hmOfCompositeState.get(i));
                write(SM_class, ".name(\""+compositeName(hmOfCompositeState.get(i), isNameComposite)+"\");\n", Boolean.TRUE);
                /************************************************************
                 *   initialize_SM() :: Composite states :: initial state
                 ***********************************************************/
                initialState(hmOfCompositeState.get(i));
                /************************************************************
                 *   initialize_SM() :: Composite states :: state invariant
                 ***********************************************************/
                stateInvariant(hmOfCompositeState.get(i));
                /****************************************************************
                 * initialize_SM() :: Composite states :: Deep & Shallow history
                 ****************************************************************/
                deepAndShallowHistory(hmOfCompositeState.get(i));
                /************************************************************
                 *    initialize_SM() :: Composite states :: output state
                 ***********************************************************/
                outputState(hmOfCompositeState.get(i));
                /************************************************************
                 *              initialize_SM() :: allowedEvent 
                 ***********************************************************/
                allowedEvent(hmOfCompositeState.get(i));
                /************************************************************
                 *      initialize_SM() :: Composite states :: set_ENTRY
                 ***********************************************************/
                action(hmOfCompositeState.get(i), hmOfCompositeState.get(i).getEntry(), "entry");
                /************************************************************
                 *      initialize_SM() :: Composite states :: set_EXIT
                 ***********************************************************/
                action(hmOfCompositeState.get(i), hmOfCompositeState.get(i).getExit(), "exit");
                /************************************************************
                 *     initialize_SM() :: Composite states :: doACTIVITY
                 ***********************************************************/
                action(hmOfCompositeState.get(i), hmOfCompositeState.get(i).getDoActivity(), "doActivity");
                /************************************************************
                 *      initialize_SM() :: set_ENTRY emptyTransition
                 ***********************************************************/
                setEntry_EmptyTransition(hmOfCompositeState.get(i));
            }
        }
        /************************************************************
         *      initialize_SM() :: STATE MACHINE initialization
         ***********************************************************/
        if(!listOfVerticesInsideSM.isEmpty()) {
            if(viewer) // if user has chosen to activate the viewer
                write(SM_class, "\t\tcom.pauware.pauware_view.PauWare_view pv = new com.pauware.pauware_view.PauWare_view();\n",true);
            
            write(SM_class, "\t\t"+SM_name+" = new ", Boolean.TRUE);
            if(!EEorME) // if user has chosen Java ME version
                write(SM_class, "Java_ME", Boolean.TRUE);
            write(SM_class, "Statechart_monitor((", Boolean.TRUE);
            
            for(int i = 0; i<listOfVerticesInsideSM.size(); i++) {
                write(SM_class, removeSpace(listOfVerticesInsideSM.get(i)), Boolean.TRUE);
                if(i == (listOfVerticesInsideSM.size()-1)) {
                    for(int j = 0; j < listOfVerticesInsideSM.size();j++) {
                        write(SM_class, ")", Boolean.TRUE);
                    }
                    write(SM_class, ", \""+SM_name+"\", AbstractStatechart_monitor.", Boolean.TRUE);
                    
                    if(showOnSystemOut) {
                        write(SM_class, "Show_on_system_out", Boolean.TRUE);
                    } else {
                        write(SM_class, "Don_t_show_on_system_out", Boolean.TRUE);
                    }
                    
                    if(viewer) 
                        write(SM_class, ", pv", Boolean.TRUE);
                    
                    write(SM_class, ");\n", Boolean.TRUE);
                } else {
                    write(SM_class, ".xor(", true);
                }
            }
        }
        write(SM_class,"\t}\n",true);
        /************************************************************
         *                         start() 
         ***********************************************************/
        write(SM_class,"\n\tpublic void start() throws Statechart_exception {\n",true);
        for(Transition t : trans_list) {
            if(!(t.getTarget() instanceof FinalState)) { // if transition is not heading a final state
                /************************************************************
                 *                start() :: STATE -> ?
                 ***********************************************************/
                if(t.getSource() instanceof State)
                    firesFromState(t);
                if(t.getSource() instanceof Pseudostate)
                    firesFromPseudostate(t);
            }
        }
        write(SM_class,"\t\t"+SM_name+".start();\n\t}\n",true);
        /************************************************************
         *                         stop() 
         ***********************************************************/
        write(SM_class, "\n\tpublic void stop() throws Statechart_exception {\n\t\t"+ SM_name+".stop();\n\t}\n", Boolean.TRUE);
        /************************************************************
         *                State machine constructor 
         ***********************************************************/
        write(SM_class, "\n\tpublic "+SM_class+"() throws Statechart_exception {\n\t\tinitialize_SM();\n\t}\n", Boolean.TRUE);
        /************************************************************
         *                         Events 
         ***********************************************************/
        if(atLeastOneEvent) {
            write(SM_class, "\n\t// Events\n", Boolean.TRUE);
            // For each t part of trans_list with state as a source and not a FinalState as a target
            trans_list.stream().filter((transition) -> (!transition.getTriggers().isEmpty() && transition.getSource() instanceof State && !(transition.getTarget() instanceof FinalState))).forEachOrdered((transition) -> {
                transition.getTriggers().forEach((trigger) -> {
                    if(trigger.getEvent() != null) {
                        // if the trigger has an event that is not part of the list of event yet we add it
                        if(!list_event.contains(trigger.getEvent().getName())) {
                            list_event.add(trigger.getEvent().getName());
                        }
                    } else {
                        // if the trigger doesn't have an event and is not part of the list of event yet we add it
                        if(!list_event.contains(trigger.getName())) {
                            list_event.add(trigger.getName());
                        }
                    }
                });
            });
            for(int i = 0; i<list_event.size(); i++) {
                event(list_event.get(i));
            }
        }
        /************************************************************
         *                   Empty transition
         ***********************************************************/
        trans_list.stream().filter((transition) -> (transition.getSource() instanceof State)).filter((transition) -> (transition.getTriggers().isEmpty())).filter((transition) -> (!(transition.getTarget() instanceof FinalState))).forEachOrdered((transition) -> {
            if(transition.getSource().getOutgoings().size() > 1) {
                if(transition.getSource().getOutgoings().get(0).equals(transition))
                    write(SM_class, "\tpublic void emptyTransition_"+compositeName((State)transition.getSource(), isNameComposite)+"() throws Statechart_exception  {\n\n\t\t"+SM_name+".run_to_completion(\"emptyTransition_"+compositeName((State)transition.getSource(), isNameComposite)+"\");\n\n\t}\n", true);
            } else {
                write(SM_class, "\tpublic void emptyTransition_"+compositeName((State)transition.getSource(), isNameComposite)+"() throws Statechart_exception  {\n\n\t\t"+SM_name+".run_to_completion(\"emptyTransition_"+compositeName((State)transition.getSource(), isNameComposite)+"\");\n\n\t}\n", true);
            }
        });
        /************************************************************
         *                  Pseudostate behaviour 
         ***********************************************************/
        pseudostate_list.forEach((pseudostate) -> {
            if(pseudostate.getKind().toString().equals("choice"))
                write(SM_class, "\tpublic void "+pseudoStateNameGenerator(pseudostate)+"() throws Statechart_exception  {\n\n\t\t//complete choice pseudostate behaviour\n\n\t\t"+SM_name+".run_to_completion(\""+pseudoStateNameGenerator(pseudostate)+"\");\n\n\t}\n", true);
            if(pseudostate.getKind().toString().equals("junction"))
                write(SM_class, "\tpublic void "+pseudoStateNameGenerator(pseudostate)+"() throws Statechart_exception  {\n\n\t\t"+SM_name+".run_to_completion(\""+pseudoStateNameGenerator(pseudostate)+"\");\n\n\t}\n", true);
            if(pseudostate.getKind().toString().equals("fork"))
                write(SM_class, "\tpublic void "+pseudoStateNameGenerator(pseudostate)+"() throws Statechart_exception  {\n\n\t\t"+SM_name+".run_to_completion(\""+pseudoStateNameGenerator(pseudostate)+"\");\n\n\t}\n", true);
            if(pseudostate.getKind().toString().equals("join"))
                write(SM_class, "\tpublic void "+pseudoStateNameGenerator(pseudostate)+"() throws Statechart_exception  {\n\n\t\t"+SM_name+".run_to_completion(\""+pseudoStateNameGenerator(pseudostate)+"\");\n\n\t}\n", true);
            if(pseudostate.getKind().toString().equals("terminate"))
                write(SM_class, "\tpublic void "+pseudoStateNameGenerator(pseudostate)+"() throws Statechart_exception  {\n\n\t\t_"+SM_name+".stop();\n\n\t}\n", true);
        });
        /************************************************************
         *                         Guards 
         ***********************************************************/
        if(!inv_list.isEmpty())
            write("BusinessGuards", "public class BusinessGuard {\n\n",true);
        for(int i = 0; i<inv_list.size(); i++){
            write("BusinessGuards", "\tpublic boolean ",true);
            write("BusinessGuards", translateBoolean(inv_list.get(i).getSpecification().stringValue()), true);
            write("BusinessGuards", "() {\n\t\t// "+inv_list.get(i).getSpecification().stringValue()+"\n\t\treturn true;\n\t}\n\n",true);
        }
        // special case of join
        pseudostate_list.forEach((pseudostate) -> {
            if(pseudostate.getKind().toString().equals("join")) {
                write("BusinessGuards", "\tpublic boolean guard_"+ pseudoStateNameGenerator(pseudostate)+"(){\n\t\tif(", true);
                if(pseudostate.getIncomings().size() >= 2) {
                    for(int i = 0; i < pseudostate.getIncomings().size(); i++) {
                        if(i == 0) {
                            write("BusinessGuards", "in_state(\""+removePunctuation(removeSpace(pseudostate.getIncomings().get(i).getSource().getName()))+"\")", true);
                        } else if(i < pseudostate.getIncomings().size()-1 && i != 0) {
                            write("BusinessGuards", "&& in_state(\""+removePunctuation(removeSpace(pseudostate.getIncomings().get(i).getSource().getName()))+"\")", true);
                        } else {
                            write("BusinessGuards", "&& in_state(\""+removePunctuation(removeSpace(pseudostate.getIncomings().get(i).getSource().getName()))+"\")) {\n\t\t\treturn true;\n\t\t} else {\n\t\t\treturn false;\n\t\t}\n\t}\n", true);
                        }
                    }
                } else {
                    write("BusinessGuards", "true)\n\t\t\treturn true;\n\t}\n", true);
                }
            }
        });
        if(!inv_list.isEmpty())
            write("BusinessGuards", "}",true);
        /************************************************************
         *                         Timer 
         ***********************************************************/
        if(timer){
            write(SM_class, "\n\t@Override\n\tpublic void time_out(long delay, AbstractStatechart context) throws Statechart_exception {\n\t\tthrow new UnsupportedOperationException(\"Not supported yet.\");\n\t}\n" +
                    "\n\t@Override\n" +
                    "\tpublic void time_out_error(Statechart_exception se) throws Statechart_exception {\n" +
                    "\t\tthrow new UnsupportedOperationException(\"Not supported yet.\");\n" +
                    "\t}\n", true);
        }
        
        if(isManageableNeeded) {
            write(SM_class, "\n\t@Override\n" +
                    "\tpublic String async_current_state() {\n" +
                    "\t\treturn _"+SM_name+".async_current_state();\n" +
                    "\t}\n" +
                    "\n" +
                    "\t@Override\n" +
                    "\tpublic String current_state() {\n" +
                    "\t\treturn _"+SM_name+".current_state();\n" +
                    "\t}\n" +
                    "\n" +
                    "\t@Override\n" +
                    "\tpublic boolean in_state(String name) {\n" +
                    "\t\treturn _"+SM_name+".in_state(name);\n" +
                    "\t}\n" +
                    "\n" +
                    "\t@Override\n" +
                    "\tpublic String name() {\n" +
                    "\t\treturn _"+SM_name+".name();\n" +
                    "\t}\n" +
                    "\n" +
                    "\t@Override\n" +
                    "\tpublic String verbose() {\n" +
                    "\t\treturn _"+SM_name+".verbose();\n" +
                    "\t}\n" +
                    "\n" +
                    "\t@Override\n" +
                    "\tpublic void to_state(String name) throws Statechart_exception {\n" +
                    "\t\t_"+SM_name+".to_state(name);\n" +
                    "\t}\n", true);
        }
        /************************************************************
         *                Business objects 
         ***********************************************************/
        it = hm_operations.keySet().iterator();
        while(it.hasNext()) {
            operation = (Operation) it.next();
            if(hm_operations.get(operation)) 
                op_list.add(operation);
        }
        /* Head of interfaces and listing of them */
        for(int i = 0; i < op_list.size();i++) {
            p = (PackageableElement) op_list.get(i).getOwner();
            if(!p.getName().equals(SM_class) || !(p.getName() == null)) {
                if(p.getName().equals("this")) {
                    write(p.getName(),"public interface IBusinessObject {\n", false);
                } else {
                    write(p.getName(),"public interface "+removeSpace(p.getName())+" {\n", false);
                }
                if(!list_interface.contains(p.getName())) 
                    list_interface.add(p.getName());
            }
        }
        /* Body of interfaces */
        for(int i = 0; i < op_list.size();i++) {
            hasReturnValue = false;
            p = (PackageableElement) op_list.get(i).getOwner();
            /* public, private, protected */
            // we write in the interface
            write(p.getName(), "\n\t"+op_list.get(i).getVisibility().toString()+" ", true);
            // we write in the interface
            sb.append("\n\t").append(op_list.get(i).getVisibility().toString()).append(" ");
            /* type of the method */
            if(op_list.get(i).getType() != null) { // procedure vs function
                write(p.getName(), pickType(op_list.get(i).getType().toString()), true);
                sb.append(pickType(op_list.get(i).getType().toString()));
            } else {
                write(p.getName(), "void", true);
                sb.append("void");
            }
            /* name of the method */
            write(p.getName(), " "+removeSpace(op_list.get(i).getName())+"(", true);
            sb.append(" ").append(removeSpace(op_list.get(i).getName())).append("(");
            /* parameters */
            if(!op_list.get(i).getOwnedParameters().isEmpty()) {
                // first parameter
                param = op_list.get(i).getOwnedParameters().get(0);
                if(!param.getDirection().toString().equals("return")) {
                    write(p.getName(),pickType(param.getType().toString())+" "+removeSpace(param.getName()),true);
                    sb.append(pickType(param.getType().toString())).append(" ").append(removeSpace(param.getName()));
                    // other parameters
                    for(int j = 1; j<op_list.get(i).getOwnedParameters().size(); j++) {
                        param = op_list.get(i).getOwnedParameters().get(j);
                        if(!param.getDirection().toString().equals("return")) {
                            write(p.getName(),", "+pickType(param.getType().toString())+" "+removeSpace(param.getName()),true);
                            sb.append(", ").append(pickType(param.getType().toString())).append(" ").append(removeSpace(param.getName()));
                        } else {
                            hasReturnValue = true;
                        }
                    }
                } else {
                    hasReturnValue = true;
                }
            }
            write(p.getName(), ");\n",true);
            sb.append(") {\n\t\t// To implement\n");
            if(hasReturnValue) {
                sb.append("\t\treturn null;\n");
            }
            sb.append("\t}\n");
        }
        for(int i = 0; i<list_interface.size();i++) {
            write(list_interface.get(i),"\n}", true);
        }
        businessMethods = sb.toString();
        write(SM_class, "\n}", Boolean.TRUE);
    }
    
    /**
     * Return the first state machine that has been found.
     * @param e the root element
     */
    public void getStateMachine(Element e) {
        if (sm != null) return;
        
        if(e instanceof StateMachine) {
            sm = (StateMachine)e;
            return;
        }
        for(Element elt : e.getOwnedElements()) {
            getStateMachine(elt);
        }
    }
    
    /**
     * Add to the list of operations all the operation that has been found in the xmi file.
     * @param e the root element
     */
    public void getOperation(Element e) {
        if(e instanceof Operation) {
            hm_operations.put((Operation) e, false);
            return;
        }
        
        for(Element elt : e.getOwnedElements()) {
            getOperation(elt);
        }
    }
    
    /**
     * Main method to call in order to generate the PauWare code.
     * @return The generated code printed in a string.
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public String processCodeGeneration() throws FileNotFoundException, IOException {
        
        /* Init */
        String xmi, buf;
        StringBuilder sb = new StringBuilder();
        List<String> l = new ArrayList<>();
        FileReader fr;
        BufferedReader br;
        FileWriter fw;
        BufferedWriter bw;
        Boolean b;
        EObject obj;
        Model model;
        PackageableElement p;
        Constraint c;
        Pseudostate ps;
        State s;
        Region r;
        Element e, eRegion, eState;
        Resource resource;
        TreeIterator<EObject> it;
        Iterator<Element> itRegion, itState, itElt;
        Iterator<Operation> itOp;
        Operation operation;
        
        /* Body */
        xmi = path + xmiName;
        fr = new FileReader(xmi);
        br = new BufferedReader(fr);
        while((buf = br.readLine()) != null){
            sb.append(buf);
        }
        // correction for Modelio
        buf = sb.toString().replaceAll("xmlns:uml=\"http://www.eclipse.org/uml2/3.0.0/UML\"", "xmlns:uml=\"http://www.eclipse.org/uml2/5.0.0/UML\"");
        // correction for StarUML
        buf = buf.replaceAll("xmlns:uml=\"http://schema.omg.org/spec/UML/2.0\"", "xmlns:uml=\"http://www.eclipse.org/uml2/5.0.0/UML\"");
        try { 
            fw = new FileWriter(xmi, false); 
            bw = new BufferedWriter(fw); 
            bw.write(buf); 
            bw.flush();
            bw.close();
            fw.flush();
            fw.close();
        } catch(IOException ioe){ 
            System.err.print("ERREUR write : " + ioe); 
        } 
        resource = chargerModele(xmi, UMLPackage.eINSTANCE);
        it = resource.getAllContents();
        while(it.hasNext()) {
            obj = (EObject)it.next();
            if (obj instanceof Model) {
                model = (Model)obj;
                
                getStateMachine(model);
                if (sm != null) {
                    p = sm;
                    SM_name = removeSpace(p.getName());
                    SM_class = firstLetterLowerOrUpperCase(removeSpace(SM_name), false);
                    itRegion = p.getOwnedElements().iterator();
                    while (itRegion.hasNext()) {
                        eRegion = itRegion.next();
                        if (eRegion instanceof Region) {
                            r = (Region) eRegion;
                            region(r);
                            itState = r.getOwnedElements().iterator();
                            while (itState.hasNext()) {
                                eState = itState.next();
                                if (eState instanceof State && !(eState instanceof FinalState)) {
                                    s = (State) eState;
                                    l.add(compositeName(s, false));
                                } else if (eState instanceof Pseudostate) {
                                    ps = (Pseudostate) eState;
                                    if (!ps.getKind().toString().equals("initial") && !ps.getKind().toString().equals("final") && !ps.getKind().toString().equals("deepHistory") && !ps.getKind().toString().equals("shallowHistory")) {
                                        l.add(pseudoStateNameGenerator(ps));
                                    }
                                }
                            }
                            listOfVerticesInsideSM = l;
                        }
                    }
                }
                getOperation(model);
                if(!hm_operations.isEmpty()) {
                    itOp = hm_operations.keySet().iterator();
                    while(itOp.hasNext()) {
                        operation = (Operation) itOp.next();
                        p = (PackageableElement) operation.getOwner();
                        if((p.getName() != null) && businessObject == false)
                            businessObject = true;
                    }
                }
                itElt = model.getOwnedElements().iterator();
                while(itElt.hasNext()) {
                    e = itElt.next();
                    if(e instanceof Constraint) {
                        c = (Constraint)e;
                        if(!inv_list.contains(c)) {
                            b = false;
                            for(int i=0; i<inv_list.size(); i++) {
                                if(inv_list.get(i).getSpecification().stringValue().equals(c.getSpecification().stringValue()))
                                    b = true;
                            }
                            if(!b)
                                inv_list.add(c);
                        }
                    }
                }
                // if there is at least one invariant or guard in the state machine, then we will need "Business guard bg;"
                if(inv_list != null && inv_list.size() > 0) {
                    businessGuard = true;
                }
                // if there is at least one transition with an event, then atLeastOneEvent is true
                if(trans_list != null && trans_list.size() > 0) {
                    for(Transition transition : trans_list) {
                        if(!transition.getTriggers().isEmpty()) {
                            atLeastOneEvent = true;
                        }
                    }
                }
            }
        }
        print();
        return stringmap.get(SM_class);
    }
    
    /**
     * 
     * @return
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public List<String> BOCodeGen() throws FileNotFoundException, IOException {
        /* Init */
        PackageableElement p;
        String currentOpOwner;
        Boolean isOpOwnerNew;
        StringBuilder businessClass = new StringBuilder();
        List<String> returnList = new ArrayList<>();
        
        /* Body */
        if(!inv_list.isEmpty())
            returnList.add(stringmap.get("BusinessGuards"));
        if(!op_list.isEmpty()) {
            businessClass.append("public class BusinessObject implements ");
            
            for(int i = 0; i < op_list.size(); i++) {
                p = (PackageableElement) op_list.get(i).getOwner();
                currentOpOwner = p.getName();
                isOpOwnerNew = true;
                // boucle for qui permet de vérifier si le propriétaire de l'opération a déjà été présent dans la liste ou non
                for(int j=0; j<i; j++) {
                    p = (PackageableElement) op_list.get(j).getOwner();
                    if(currentOpOwner.equals(p.getName()))
                        isOpOwnerNew = false;
                }
                // si le propriétaire de l'opération n'a pas encoré été traité
                if(isOpOwnerNew) {
                    p = (PackageableElement) op_list.get(i).getOwner();
                    if(i == 0) {
                        if(p.getName().equals("this")) {
                            businessClass.append("IBusinessObject");
                        } else {
                            businessClass.append(p.getName());
                        }
                    } else {
                        if(p.getName().equals("this")) {
                            businessClass.append(", IBusinessObject");
                        } else {
                            businessClass.append(", ").append(p.getName());
                        }
                    }
                    returnList.add(stringmap.get(p.getName()));
                }
            }
            businessClass.append(" {\n").append(businessMethods).append("\n}");
            returnList.add(businessClass.toString());
        }
        return returnList;
    }
}
