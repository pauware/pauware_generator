package pauwarecodegeneration.servlets;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import pauwarecodegeneration.CodeGeneration;

/**
 *
 * @author Lea Brunschwig
 */
@WebServlet(name = "PauWareServlet", urlPatterns = {"/PauWareServlet"})
public class PauWareServlet extends HttpServlet {
    
    public static final int BUFFER_SIZE = 10240;
    //public static final String FILE_PATH = "/web/stockage/";

    public PauWareServlet() {
        super();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        this.getServletContext().getRequestDispatcher("/WEB-INF/PauWare.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        /* Init */
        Part part;
        String nomFichier, nomChamp;
        CodeGeneration cgc;
        Boolean EEorME, viewer, trace, timer;
        String path = request.getSession().getServletContext().getRealPath("/");
        
        /*Body */
        part = request.getPart("xmifile");
        nomFichier = getNomFichier(part);
        // Si on a bien un fichier
        if (nomFichier != null && !nomFichier.isEmpty()) {
            nomChamp = part.getName();
            // Corrige un bug du fonctionnement d'Internet Explorer
            nomFichier = nomFichier.substring(nomFichier.lastIndexOf('/') + 1)
                    .substring(nomFichier.lastIndexOf('\\') + 1);
            // On écrit définitivement le fichier sur le disque
            //ecrireFichier(part, nomFichier, FILE_PATH);
            ecrireFichier(part, nomFichier, path);
            request.setAttribute(nomChamp, nomFichier);
            // on transforme les strings en boolean
            EEorME = request.getParameter("EEorME").equals("true");
            viewer = request.getParameter("viewer").equals("true");
            trace = request.getParameter("trace").equals("true");
            timer = request.getParameter("timer").equals("true");
            // on applique le traitement sur le fichier pour la generation de code
            cgc = new CodeGeneration(nomFichier, EEorME, viewer, trace, timer, path);
            request.setAttribute("result", cgc.processCodeGeneration());
            //request.setAttribute("resultBO", cgc.processBOCodeGen());
            request.setAttribute("resultTest", cgc.BOCodeGen());
            this.getServletContext().getRequestDispatcher("/WEB-INF/PauWare.jsp").forward(request, response);
        } else {
            this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
        }
        
    }
    
     private void ecrireFichier( Part part, String nomFichier, String chemin ) throws IOException {
        BufferedInputStream entree = null;
        BufferedOutputStream sortie = null;
        try {
            entree = new BufferedInputStream(part.getInputStream(), BUFFER_SIZE);
            sortie = new BufferedOutputStream(new FileOutputStream(new File(chemin + nomFichier)), BUFFER_SIZE);

            byte[] tampon = new byte[BUFFER_SIZE];
            int longueur;
            while ((longueur = entree.read(tampon)) > 0) {
                sortie.write(tampon, 0, longueur);
            }
        } finally {
            try {
                sortie.close();
            } catch (IOException ignore) {
            }
            try {
                entree.close();
            } catch (IOException ignore) {
            }
        }
    }
    
    private static String getNomFichier( Part part ) {
        for ( String contentDisposition : part.getHeader( "content-disposition" ).split( ";" ) ) {
            if ( contentDisposition.trim().startsWith( "filename" ) ) {
                return contentDisposition.substring( contentDisposition.indexOf( '=' ) + 1 ).trim().replace( "\"", "" );
            }
        }
        return null;
    }

}
