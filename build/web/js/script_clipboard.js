$(document).ready(function(){
    
    /* initialize clipboardjs */
    var clipboard = new ClipboardJS(".copy-btn",{
        target:function(trigger){
            return trigger.previousElementSibling;
        }
    });
    
    /* for tooltip */
    $('[data-toggle="tooltip"]').toArray().forEach(function(currentItem){
        $(currentItem).tooltip({
            title:"Copy to Clipboard",
            delay:{
                "show":500,
                "hide":500
            },
            trigger:"hover"
        }).on("click", function(){
            $(this).attr("data-original-title","Copied!")
                    .tooltip("show")
                    .on("hidden.bs.tooltip", function(){
                        $(this).attr("data-original-title","Copy to Clipboard");
            });
        });
    });
});

