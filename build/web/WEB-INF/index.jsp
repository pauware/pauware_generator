<%-- 
    Document   : index
    Created on : 5 nov. 2018, 14:35:31
    Author     : Lea Brunschwig
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PauWare Code Generator</title>
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <meta property="og:title" content="PauWare Generator">
        <meta property="og:site_name" content="PauWare"/>
        <meta property="og:url" content="http://www.pauware.com/generator" />
        <meta property="og:locale" content="fr">
        <meta property="og:type" content="website" />
        
        <link rel="icon" href="./images/favicon.ico">
        
        <link rel="stylesheet" href="./styles/bootstrap.min.css">
        <link rel="stylesheet" href="./styles/default.css">
        <link rel="stylesheet" href="./styles/font-awesome.css">
        <link rel="stylesheet" href="./styles/style.css">
        
        <link rel="stylesheet" href="./styles/style_balloon.css">
        <link rel="stylesheet" href="./styles/style_toggleswitch.css">
        
        <script src="./js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class ="jumbotron text-center">
            <h1>PauWare Code Generator</h1>
            <p>Upload your XMI file and get your generated PauWare state machine code.</p>
            <a href="help.html">How to use it?</a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                
                </div>
                <div class="col-sm-8">
                    <form class="form-horizontal" method="POST" action="pauware" enctype="multipart/form-data" id="form">
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="xmifile" name="xmifile" aria-describedby="xmifile" accept=".xmi" required>
                            </div>
                        </div>
                        </br>
                        <div class="form-group">
                            <label class="control-label col-sm-4 font-weight-bold" for="EEorME" data-balloon-length="fit" data-balloon="Choose either you want a Java EE PauWare generated code file or Java ME." data-balloon-pos="left">Java EE or ME:</label>
                            <label class="radio-inline col-sm-1"><input type="radio" id="EEorME" name="EEorME" value="true" checked/> EE </label>
                            <label class="radio-inline"><input type="radio" id="EEorME" name="EEorME" value="false"/> ME </label>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4 font-weight-bold" for="viewer" data-balloon-length="fit" data-balloon="Choose either you want to add the PauWare viewer functionality to the generated code or not. Note that you need to install the PauWare viewer to implement this option." data-balloon-pos="left">Viewer:</label>
                            <label class="radio-inline col-sm-1"><input type="radio" id="viewer" name="viewer" value="true" /> On </label>
                            <label class="radio-inline"><input type="radio" id="viewer" name="viewer" value="false" checked/> Off </label>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4 font-weight-bold" for="trace" data-balloon-length="fit" data-balloon="Choose either you want to show the trace in the console during execution or not." data-balloon-pos="left">Trace:</label>
                            <label class="radio-inline col-sm-1"><input type="radio" id="trace" name="trace" value="true"/> On </label>
                            <label class="radio-inline"><input type="radio" id="trace" name="trace" value="false" checked/> Off </label>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4 font-weight-bold" for="timer" data-balloon-length="fit" data-balloon="This option allows you to add the timing feature of PauWare if your application needs it." data-balloon-pos="left">Timer:</label>
                            <label class="radio-inline col-sm-1"><input type="radio" id="timer" name="timer" value="true"/> On </label>
                            <label class="radio-inline"><input type="radio" id="timer" name="timer" value="false" checked/> Off </label>
                        </div>
                        <div class="form-group"> 
                            <div class="col-sm-10">
                                </br>
                                <button type="submit">Submit</button>
                            </div>
                        </div>
                        <hr>
                        <p id='jar'>Jar file you need to add to your project: <a id="ajar" href="jar/pauware_java_ee.jar">pauware_java_ee.jar</a></p>
                        <script>
                            // get list of radio buttons with name 'size'
                            var EEorME = document.forms['form'].elements['EEorME'];
                            var p = document.getElementById('jar');

                            // loop through list
                            for (var i=0, length=EEorME.length; i<length; i++) {
                                EEorME[i].onclick = function() { // assign onclick handler function to each
                                    // put clicked radio button's value in total field
                                    if(this.value === 'true') {
                                        p.innerHTML = 'Jar file you need to add to your project: <a id="ajar" href="jar/pauware_java_ee.jar">pauware_java_ee.jar</a>';
                                    } else if(this.value === 'false'){
                                        p.innerHTML = 'Jar file you need to add to your project: <a id="ajar" href="jar/pauware_java_me.jar">pauware_java_me.jar</a>';
                                    }
                                };
                            }
                        </script>
                    </form>
                </div>
                <div class="col-sm-3">
                </div>
            </div>
        </div>
        
        <footer class="footer hidden-print">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="pull-left">

                        </div>
                        <div class="pull-right">

                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="container copyright">
                            <small>
                                &copy; 2019 Copyright <a href="http://www.pauware.com">PauWare</a>.
                            </small>
                        </div>
                        <div class="container copyright tm-info">
                            <small>
                                This website is hosted by University of Pau
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        
        
        <script>
            $("input[type=file]").change(function (e){$(this).next('.custom-file-label').text(e.target.files[0].name);})
        </script>
        
        <style>
            #form {
                position:relative;
                top:50%;
                left:50%;
                width:450px; /* A toi de donner la bonne largeur */
                height:300px; /* A toi de donner la bonne hauteur */
                margin-left:-225px; /* -largeur/2 */
                margin-top:-50px; /* -hauteur/2 */
                font-size: 20px;
                text-align:center
             }
        </style>
    </body>
</html>
