<%-- 
    Document   : PauWare
    Created on : 26 oct. 2018, 16:54:49
    Author     : Lea Brunschwig
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PauWare Code Generator</title>
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <meta property="og:title" content="PauWare Generator">
        <meta property="og:site_name" content="PauWare"/>
        <meta property="og:url" content="http://www.pauware.com/generator" />
        <meta property="og:locale" content="fr">
        <meta property="og:type" content="website" />
        
        <link rel="icon" href="./images/favicon.ico">
        
        <link rel="stylesheet" href="./styles/bootstrap.min.css">
        <link rel="stylesheet" href="./styles/default.css">
        <link rel="stylesheet" href="./styles/font-awesome.css">
        <link rel="stylesheet" href="./styles/style.css">
        
        <link rel="stylesheet" href="./styles/style_balloon.css">
        <link rel="stylesheet" href="./styles/style_toggleswitch.css">
        
        <link rel="stylesheet" href="./styles/atom-one-light.css">        

        
        <script src="./js/highlight.pack.js"></script>
        <script>hljs.initHighlightingOnLoad();</script>
    
        
    </head>
    <body>
        <header id="main-header">
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-left">
                            <li></li>
                            <li>
                                <a href="index" style="color: darkslategrey">
                                    Back
                                </a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a style="color: darkslategrey">PauWare Code Generator</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                
                </div>
                <div class="col-sm-7">
                    </br>
                </div>
                <div class="col-sm-3">
                
                </div>
            </div>
            <div class="row">
                <div class="col-sm-1">
                    
                </div>
                <div class="col-sm-10">
                    <c:if test="${!empty xmifile}"><p><c:out escapeXml="false" value="<div class=\"form-group green-border-focus\">
                                                                                        <pre><code class=\"language-java\" id=\"info\">${result}</code></pre>
                                                                                        <button class=\"copy-btn\" data-clipboard-target=\"#info\" data-toggle=\"tooltip\" data-placement=\"bottom\">Copy</button><div id=\"code\"></div></div>"/></p></c:if>
                       
                <p><c:forEach items="${resultTest}" var="testResult"><div class="form-group green-border-focus">
                                                                                        <pre><code class="language-java" id="info">${testResult}</code></pre>
                                                                                        <button class="copy-btn" data-clipboard-target="#info" data-toggle="tooltip" data-placement="bottom">Copy</button><div id="bo"></div></div></p></c:forEach>                                                                 
                
                </div>
                <div class="col-sm-1">
                
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                
                </div>
                <div class="col-sm-6">
                
                </div>
                <div class="col-sm-3">
                
                </div>
            </div>
        </div>
        
        <footer class="footer hidden-print">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="pull-left">

                        </div>
                        <div class="pull-right">

                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="container copyright">
                            <small>
                                &copy; 2019 Copyright <a href="http://www.pauware.com">PauWare</a>.
                            </small>
                        </div>
                        <div class="container copyright tm-info">
                            <small>
                                This website is hosted by University of Pau
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
                    
           
        
        <!-- <script src="./js/bootstrap.min.js"></script> -->           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.1/clipboard.min.js">
            new ClipboardJS('.btn');
        </script>
        <script src="./js/script_clipboard.js"></script>
    </body>
</html>
