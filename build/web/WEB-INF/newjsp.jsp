<%-- 
    Document   : index
    Created on : 5 nov. 2018, 14:35:31
    Author     : Lea Brunschwig
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PauWare</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="styles/style_balloon.css">
        <link rel="stylesheet" href="styles/style_toggleswitch.css">
    </head>
    <body>
        <div class ="jumbotron text-center">
            <h1>PauWare Code Generator</h1>
            <p>Upload your XMI file and get your generated PauWare state machine code.</p>
            
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <form class="form-horizontal" method="POST" action="pauware" enctype="multipart/form-data" id="form">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="xmifile">Upload</span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="xmifile" name="xmifile" aria-describedby="xmifile" accept=".xmi" required>
                                <label class="custom-file-label" for="xmifile">Choose an .xmi file (EMF format)</label>
                            </div>
                        </div>
                        <div class="form-group" id="lol">
                            <label class="control-label col-sm-4 font-weight-bold" for="EEorME" data-balloon-length="fit" data-balloon="Choose either you want a Java EE PauWare generated code file or Java ME." data-balloon-pos="left">Java EE or ME:</label>
                            <label class="radio-inline col-sm-3"><input type="radio" id="EEorME" name="EEorME" value="true" checked/> EE </label>
                            <label class="radio-inline"><input type="radio" id="EEorME" name="EEorME" value="false"/> ME </label>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4 font-weight-bold" for="viewer" data-balloon-length="fit" data-balloon="Choose either you want to add the PauWare viewer functionality to the generated code or not. Note that you need to install the PauWare viewer to implement this option." data-balloon-pos="left">Viewer:</label>
                            <label class="radio-inline col-sm-3"><input type="radio" id="viewer" name="viewer" value="true" /> On </label>
                            <label class="radio-inline"><input type="radio" id="viewer" name="viewer" value="false" checked/> Off </label>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4 font-weight-bold" for="trace" data-balloon-length="fit" data-balloon="Choose either you want to show the trace in the console during execution or not." data-balloon-pos="left">Trace:</label>
                            <label class="radio-inline col-sm-3"><input type="radio" id="trace" name="trace" value="true"/> On </label>
                            <label class="radio-inline"><input type="radio" id="trace" name="trace" value="false" checked/> Off </label>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-4 font-weight-bold" for="timer" data-balloon-length="fit" data-balloon="This option allows you to add the timing feature of PauWare if your application needs it." data-balloon-pos="left">Timer:</label>
                            <label class="radio-inline col-sm-3"><input type="radio" id="timer" name="timer" value="true"/> On </label>
                            <label class="radio-inline"><input type="radio" id="timer" name="timer" value="false" checked/> Off </label>
                        </div>
                        <div class="form-group"> 
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                        <p id='jar'>Jar file you need to add to your project: <a id="ajar" href="jar/pauware_java_ee.jar">pauware_java_ee.jar</a></p>
                        <script>
                            // get list of radio buttons with name 'size'
                            var EEorME = document.forms['form'].elements['EEorME'];
                            var p = document.getElementById('jar');

                            // loop through list
                            for (var i=0, length=EEorME.length; i<length; i++) {
                                EEorME[i].onclick = function() { // assign onclick handler function to each
                                    // put clicked radio button's value in total field
                                    if(this.value === 'true') {
                                        p.innerHTML = 'Jar file you need to add to your project: <a id="ajar" href="jar/pauware_java_ee.jar">pauware_java_ee.jar</a>';
                                    } else if(this.value === 'false'){
                                        p.innerHTML = 'Jar file you need to add to your project: <a id="ajar" href="jar/pauware_java_me.jar">pauware_java_me.jar</a>';
                                    }
                                };
                            }
                        </script>
                    </form>
                </div>
                <div class="col-sm-3">
                </div>
            </div>
        </div>
        <script>
            $("input[type=file]").change(function (e){$(this).next('.custom-file-label').text(e.target.files[0].name);})
        </script>
    </body>
</html>
